package vault.spec.org.specvaultv2.commands;

import android.util.Base64;
import android.util.Log;

import digest.Sha256;
import vault.spec.org.specvaultv2.ApplicationController;
import vault.spec.org.specvaultv2.ApplicationModel;

/**
 * Created by radu on 09.08.2016.
 */
public class CmdHash extends AbstractCommand {

    private String mStringToHash;
    private byte[] mBytesToHash;
    private byte[] mHashedBytes;
    private String mHashedString;

    public CmdHash(String argStringToHash){
        mStringToHash = argStringToHash;
    }

    public CmdHash(byte[] argBytesToHash){
        mBytesToHash = argBytesToHash;
    }

    @Override
    public void execute(){
        Sha256 sha = new Sha256();
        if(mBytesToHash == null){
            try{
                mBytesToHash = mStringToHash.getBytes("UTF-8");
            }catch (Exception e){
                Log.e("Hash", "UTF-8");
            }
        }
        sha.update(mBytesToHash, 0, mBytesToHash.length);
        mHashedBytes = sha.digest();
        mHashedString = Base64.encodeToString(mHashedBytes, Base64.DEFAULT);
    }

    public byte[] getHashedBytes(){
        return mHashedBytes;
    }

    public String getHashedString(){
        return mHashedString;
    }

    @Override
    public String describeCommand(){
        return "Hashing password";
    }

}
