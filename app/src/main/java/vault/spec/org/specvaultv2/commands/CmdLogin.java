package vault.spec.org.specvaultv2.commands;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;

import digest.Sha256;
import vault.spec.org.specvaultv2.ApplicationModel;
import vault.spec.org.specvaultv2.util.IoUtil;

/**
 * Created by radu on 11.08.2016.
 */
public class CmdLogin extends AbstractCommand {

    String mPassword;

    public CmdLogin(Context argContext, ApplicationModel argModel, String argPassword){
        super(argContext, argModel);
        this.mPassword = argPassword;
    }

    @Override
    public void execute() throws Throwable{
        File hashFile = IoUtil.getPasswordHashFile(mContext);
        FileInputStream fis = null;
        DataInputStream dis = null;
        byte[] connPassBytes = new byte[32];
        String connPass;
        try{
            fis = new FileInputStream(hashFile);
            dis = new DataInputStream(fis);
            dis.readFully(connPassBytes);
        }catch(Throwable t){
            throw t;
        }finally{
            IoUtil.closeStream(fis);
            IoUtil.closeStream(dis);
        }
        connPass = Base64.encodeToString(connPassBytes, Base64.DEFAULT);
        byte[] inputHashedPass = hash(mPassword, 10);
        String inputHashedStringPass = Base64.encodeToString(inputHashedPass, Base64.DEFAULT);
        if(connPass.compareTo(inputHashedStringPass)!=0){
            throw new RuntimeException("Passwords missmatch.");
        }else{
            mModel.setmState(ApplicationModel.STATE_VAULT_BROWSING);
        }

    }

    public byte[] hash(String argString, int numberOfIterations){
        Sha256 sha = new Sha256();
        byte[] bytes = argString.getBytes();
        if(bytes.length > 0){
            for(int i = 0; i < numberOfIterations; i++)
                sha.update(bytes, 0, bytes.length);
            bytes = sha.digest();
        }
        return bytes;
    }

    @Override
    public String describeCommand() {
        return "Logging in...";
    }
}
