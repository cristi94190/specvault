package vault.spec.org.specvaultv2;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

public class ApplicationView extends AppCompatActivity implements View.OnClickListener,ApplicationModel.Listener {

    private static int ENCRYPT_CODE = 1;
    private static int DECRYPT_CODE = 1;
    private RelativeLayout mViewLogin;
    private RelativeLayout mViewCreatePassword;
    private RelativeLayout mViewProgress;
    private ProgressBar mProgressBar;
    private TextView mTextViewProgress;
    private RelativeLayout mViewError;
    private TextView mTextViewError;
    private RelativeLayout mViewVaultBrowsing;
    private ApplicationModel mAppModel;
    private ApplicationController mAppController;
    private String mPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAppModel = new ApplicationModel(this);
        mAppController = new ApplicationController(mAppModel,this.getApplicationContext());
        mViewLogin = (RelativeLayout) findViewById(R.id.mViewLogin);
        mViewVaultBrowsing = (RelativeLayout) findViewById(R.id.mViewVaultBrowsing);
        findViewById(R.id.mButtonLogin).setOnClickListener(this);
        findViewById(R.id.mButtonReset).setOnClickListener(this);
        mViewCreatePassword = (RelativeLayout) findViewById(R.id.mViewCreatePassword);
        findViewById(R.id.mButtonCreate).setOnClickListener(this);
        mViewProgress = (RelativeLayout) findViewById(R.id.mViewProgress);
        mProgressBar = (ProgressBar) findViewById(R.id.mProgressBar);
        mTextViewProgress = (TextView) findViewById(R.id.mTextViewProgress);
        mViewError = (RelativeLayout) findViewById(R.id.mViewError);
        findViewById(R.id.mButtonOk).setOnClickListener(this);
        ListView fileListView = (ListView)findViewById(R.id.mListViewFiles);
        mTextViewError = (TextView) findViewById(R.id.mTextViewError);
        findViewById(R.id.mButtonAdd).setOnClickListener(this);
        findViewById(R.id.mButtonRemove).setOnClickListener(this);
        mAppController.commandInitialisation();
        bindViewWithState(mAppModel.getmState());

    }

    @Override
    protected void onResume() {
        super.onResume();
        mAppController.commandInitialisation();
    }

    private EditText getMEditTextPassword() {
        return (EditText) findViewById(R.id.mEditTextPassword);
    }

    private EditText getCreatePassword1() {
        return (EditText) findViewById(R.id.CreatePassword1);
    }

    private EditText getCreatePassword2() {
        return (EditText) findViewById(R.id.CreatePassword2);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mButtonLogin:
                //TODO implement
                mAppController.commandLogin(getMEditTextPassword().getText().toString());
                mPass = getMEditTextPassword().getText().toString();
                //bindViewWithState(mAppModel.getmState());
//                if(checkPass()){
//                    mAppModel.setmState(ApplicationModel.STATE_VAULT_BROWSING);
//                    bindViewWithState(mAppModel.getmState());
//                    //LOGIN
//                    Toast.makeText(this, "Password is correct", Toast.LENGTH_SHORT).show();
//                }else{
//                    Toast.makeText(this, "Password is not correct", Toast.LENGTH_SHORT).show();
//                }

                break;
            case R.id.mButtonReset:
                //TODO implement
                mAppController.resetPass();
//                mAppModel.setmState(ApplicationModel.STATE_REGISTERING);
//                bindViewWithState(mAppModel.getmState());
            break;
            case R.id.mButtonCreate:
                mAppController.commandCreateVault(getCreatePassword1().getText().toString(),getCreatePassword2().getText().toString());
                mPass = getCreatePassword1().getText().toString();
                //bindViewWithState(mAppModel.getmState());
                //TODO implement


                break;
            case R.id.mButtonOk:
                //TODO implement
                break;
            case R.id.mButtonAdd:
                encryptFile();
                break;

            case R.id.mButtonRemove:
                decryptFile();
                break;
        }
    }


    @Override
    public void onCommandStarted(ApplicationModel argModel, String argMessage) {
        mViewProgress.setVisibility(View.VISIBLE);
        mTextViewProgress.setText(argMessage);
    }

    @Override
    public void onCommandFinished(ApplicationModel argModel, Throwable argThrowable) {
        mViewProgress.setVisibility(View.GONE);
        if(argThrowable!=null){
            Toast.makeText(getApplicationContext(),"Error:\n"+argThrowable.getMessage(),Toast.LENGTH_LONG).show();
        }
        bindViewWithState(argModel.getmState());
    }

    public void hideAllViews(){
        mViewCreatePassword.setVisibility(View.GONE);
        mViewError.setVisibility(View.GONE);
        mViewProgress.setVisibility(View.GONE);
        mViewLogin.setVisibility(View.GONE);
        mViewVaultBrowsing.setVisibility(View.GONE);
    }
    public void bindViewWithState(int argState){
        hideAllViews();
        switch (argState){
            case ApplicationModel.STATE_LOGGING_IN:
                mViewLogin.setVisibility(View.VISIBLE);
                break;
            case ApplicationModel.STATE_CHANGING_PASSWORD:
                mViewCreatePassword.setVisibility(View.VISIBLE);
                break;
            case ApplicationModel.STATE_REGISTERING:
                mViewCreatePassword.setVisibility(View.VISIBLE);
                break;
            case ApplicationModel.STATE_VAULT_BROWSING:
                mViewVaultBrowsing.setVisibility(View.VISIBLE);
                break;
            default:
                break;

        }
    }

//    public boolean checkPass(){
//        String passFromFile = mAppController.readFromFileCommand();
//        String hashedPassFromFile = mAppController.HashBytes(passFromFile);
//        String pass = getMEditTextPassword().getText().toString();
//        String hashedPass = mAppController.HashBytes(pass);
//        return (hashedPass.equals(hashedPassFromFile));
//    }

    public boolean checkStrings(String argStr1, String argStr2){
        return argStr1.equals(argStr2);
    }

//    public boolean checkFile(String argPath){
//        File file = new File(argPath);
//        return file.exists() && file.length() != 0;
//
//    }

    public void encryptFile(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("image/*");
        try {
            mAppModel.setmState(ApplicationModel.STATE_VAULT_BROWSING);
            startActivityForResult(intent, ENCRYPT_CODE);
        }catch(Throwable t){
            Toast.makeText(this, "File not OK", Toast.LENGTH_SHORT).show();
        }

    }

    public void decryptFile(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("image/*");
        try {
            mAppModel.setmState(ApplicationModel.STATE_VAULT_BROWSING);
            startActivityForResult(intent, DECRYPT_CODE);
        }catch(Throwable t){
            Toast.makeText(this, "File not OK", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri fPath = data.getData();

            if (requestCode == ENCRYPT_CODE) {
                mAppController.commandEncrypy(new File(getRealPathFromURI(fPath)), mPass);
            } else {
                mAppController.commandDecrypt(new File(getRealPathFromURI(fPath)), mPass);
            }
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    private String getRealPathFromURI(Uri contentURI) {
        String[] projection = { MediaStore.Images.Media.DATA };

        Cursor cursor = getContentResolver().query(contentURI, projection, null, null, null);
        cursor.moveToFirst();

        Log.d("URI", DatabaseUtils.dumpCursorToString(cursor));

        int columnIndex = cursor.getColumnIndex(projection[0]);
        String picturePath = cursor.getString(columnIndex); // returns null
        cursor.close();
        return picturePath;
    }
}
