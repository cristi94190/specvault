package vault.spec.org.specvaultv2.commands;

import android.content.Context;
import android.os.Environment;
import android.os.StrictMode;

import vault.spec.org.specvaultv2.ApplicationController;
import vault.spec.org.specvaultv2.ApplicationModel;

/**
 * Created by radu on 11.08.2016.
 */
public class CmdGetFiles extends AbstractCommand {

    private final static String filePath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private String mPath;

    public CmdGetFiles(Context argContext, ApplicationModel argModel, String argPath){
        super(argContext, argModel);
        this.mPath = argPath;
    }
    @Override
    public void execute() {
        String filesPath = filePath + mPath;
    }

    @Override
    public String describeCommand() {
        return null;
    }
}
