package vault.spec.org.specvaultv2.util;

/**
 * Created by Loser on 11.08.2016.
 */
public class Item implements Comparable<Item> {
    private String mName;
    private String mData;
    private String mPath;
    private String mImage;

    public Item(String argName,String argData, String argPath, String argImg){
        mName = argName;
        mData = argData;
        mPath = argPath;
        mImage = argImg;
    }
    public String getName() {
        return mName;
    }
    public String getData() {
        return mData;
    }

    public String getPath() {
        return mPath;
    }

    public String getImage() {
        return mImage;
    }
    public int compareTo(Item o) {
        if(this.mName != null)
            return this.mName.toLowerCase().compareTo(o.getName().toLowerCase());
        else
            throw new IllegalArgumentException();
    }
}