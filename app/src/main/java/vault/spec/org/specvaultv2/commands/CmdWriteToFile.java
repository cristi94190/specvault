package vault.spec.org.specvaultv2.commands;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import vault.spec.org.specvaultv2.ApplicationController;
import vault.spec.org.specvaultv2.util.IoUtil;

/**
 * Created by radu on 09.08.2016.
 */
public class CmdWriteToFile extends AbstractCommand {

    private String mPassword;
    private String mFilePath;
    private Context mContext;

    public CmdWriteToFile(String argPass, String argFilePath, Context ctx) {
        mContext = ctx;
        mPassword = argPass;
        mFilePath =  argFilePath;
    }

    @Override
    public void execute() {
        File sdcard = Environment.getExternalStorageDirectory();
        File dir = new File(sdcard.getAbsolutePath() + "/pass");
        File file = new File(dir, "pass.txt");
        FileOutputStream fos = null;
                //mContext.openFileOutput(file.getAbsolutePath(), Context.MODE_PRIVATE);
        try {
            fos = new FileOutputStream(file);
            if (isExternalStorageWritable()) {
                fos.write(mPassword.getBytes());
            } else {
                Log.i("WritingFile", "Password is not 32 bytes");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            IoUtil.closeStream(fos);
        }
    }

    @Override
    public String describeCommand() {
        return "Writing password to file ...";
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;

    }

}
