package vault.spec.org.specvaultv2.commands;

import android.content.Context;
import android.os.Environment;

import java.io.File;

import vault.spec.org.specvaultv2.ApplicationController;
import vault.spec.org.specvaultv2.ApplicationModel;
import vault.spec.org.specvaultv2.util.IoUtil;

/**
 * Created by radu on 08.08.2016.
 */
public class CmdAppInit extends AbstractCommand{

    public CmdAppInit(Context argContext, ApplicationModel argAppModel){
        super(argContext,argAppModel); //apelez constructorul din clasa de baza
    }
    @Override
    public void execute() {
        File hashFile = IoUtil.getPasswordHashFile(this.mContext);
        if(hashFile.exists()){
            this.mModel.setmState(ApplicationModel.STATE_LOGGING_IN);
        }else{
            this.mModel.setmState(ApplicationModel.STATE_REGISTERING);
        }
    }

    @Override
    public String describeCommand() {
        return "Doing awesome stuff ...";
    }
}
