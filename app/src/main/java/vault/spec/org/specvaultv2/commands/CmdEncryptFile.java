package vault.spec.org.specvaultv2.commands;

import android.content.Context;
import android.util.Log;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;

import ofbencryption.CryptoModule;
import ofbencryption.Main;
import rsa.RSA;
import rsa.key.GnuRSAPrivateKey;
import rsa.key.GnuRSAPublicKey;
import rsa.key.IKeyPairCodec;
import vault.spec.org.specvaultv2.ApplicationModel;
import vault.spec.org.specvaultv2.util.CryptoUtil;
import vault.spec.org.specvaultv2.util.IoUtil;

/**
 * Created by Loser on 12.08.2016.
 */
public class CmdEncryptFile extends AbstractCommand {
    private static int MAX_LENGTH = 20000;
    final static String iv = "00000000000000000011100000000000";
    KeyPair mKeyPair;
    File mFile;
    String mPass;

    public CmdEncryptFile(Context argContext, ApplicationModel argModel, File argFile, String argPass){
        super(argContext, argModel);
        this.mFile = argFile;
        this.mPass = argPass;

    }

    @Override
    public void execute() throws Throwable {
        BigInteger encryptedAesKey;
        BigInteger decryptedKey;
        if (checkRsaKey()) {
            mKeyPair = loadKeyPair(IoUtil.getPrivateKeyFile(mContext).getAbsolutePath(), IoUtil.getPublicKeyFile(mContext).getAbsolutePath());
        }else {
            mKeyPair = Main.RSAGenerateKeyPair();
            saveKey((GnuRSAPrivateKey) mKeyPair.getPrivate(), (GnuRSAPublicKey) mKeyPair.getPublic(), IoUtil.getPrivateKeyFile(mContext).getAbsolutePath(), IoUtil.getPublicKeyFile(mContext).getAbsolutePath());
        }

        if(checkAesKey()) {
            byte[] encryptedBytes = readBytes(IoUtil.getAesKeyFile(mContext).getAbsolutePath());
            encryptedAesKey = new BigInteger(encryptedBytes);
        }else{
            byte[] derivPassBytes = CryptoUtil.derivPass(mPass,5);
            encryptedAesKey = RSAEncrypt(derivPassBytes, mKeyPair);
            encryptedAesKey.toByteArray();
            CryptoUtil.writeToFile(mContext, encryptedAesKey.toByteArray(), IoUtil.getAesKeyFile(mContext));
        }

        decryptedKey = RSADecrypt(mKeyPair, encryptedAesKey);

        encrypt(decryptedKey.toByteArray(), mFile.getAbsolutePath(),  mFile.getAbsolutePath() + ".enc");
    }

    @Override
    public String describeCommand() {
        return "Encrypting";
    }

    public boolean checkRsaKey(){
        return IoUtil.getPrivateKeyFile(mContext).exists() && IoUtil.getPublicKeyFile(mContext).exists();
    }

    public boolean checkAesKey(){
        //check if keyFile exists
        return IoUtil.getAesKeyFile(mContext).exists();
    }

    public static KeyPair loadKeyPair(String privateKeyPath, String publicKeyPath) {
        return new KeyPair(readPublicKey(publicKeyPath), readPrivateKey(privateKeyPath));

    }

    public static RSAPublicKey readPublicKey(String filename) {
        try {
            byte[] fileBytes = readBytes(filename);
            GnuRSAPublicKey pubKey = GnuRSAPublicKey.valueOf(fileBytes);
            return pubKey;
        } catch (Throwable t) {
            Logger.getLogger(CmdEncryptFile.class.getName()).log(Level.SEVERE, null, t);
        }
        return null;
    }

    public static RSAPrivateKey readPrivateKey(String filename) {
        try {
            byte[] fileBytes = readBytes(filename);
            GnuRSAPrivateKey privKey = GnuRSAPrivateKey.valueOf(fileBytes);
            return privKey;
        } catch (Throwable t) {
            Logger.getLogger(CmdEncryptFile.class.getName()).log(Level.SEVERE, null, t);
        }
        return null;
    }

    public static byte[] readBytes(String argFilename) throws IOException {
        File f = new File(argFilename);
        FileInputStream fis = new FileInputStream(f);
        if(f.length() < MAX_LENGTH){
            try {
                DataInputStream dis = new DataInputStream(fis);
                byte[] keyBytes = new byte[(int) f.length()];
                dis.readFully(keyBytes);
                dis.close();
                return keyBytes;
            } catch (Throwable ex) {
                throw ex;
            } finally {
                fis.close();
            }
        }else   throw new RuntimeException("File is too large");
    }

    private static BigInteger RSAEncrypt(byte[] argBytes, KeyPair kp1) {

        //byte[] bytes = //PrintUtil.randomString(32).getBytes();
        BigInteger bi = new BigInteger(argBytes);
        bi = bi.abs();
        BigInteger encrypted = RSA.encrypt(kp1.getPublic(), bi);
        //System.out.println("EncryptedKey:                       " + PrintUtil.toHex(encrypted.toByteArray()));
        return encrypted;
    }

    private static BigInteger RSADecrypt(KeyPair kp1, BigInteger encrypted) {
        BigInteger decrypted = RSA.decrypt(kp1.getPrivate(), encrypted);
        return decrypted;
    }

    private  void encrypt(byte[] argKey, String argPlaintextFile, String argEncryptedFile) throws Exception {

        CryptoModule encryptionModule = new CryptoModule(true, argPlaintextFile, argEncryptedFile, argKey, iv, new CryptoModule.ProgressListener() {
            @Override
            public void onProgress(int argPercent) {
                //System.out.flush();
                //System.out.println("Encryption progress report: " + argPercent + " % .");
            }
        });
        encryptionModule.init();
        encryptionModule.process();
        //byte[] hashedFile = encryptionModule.process();
        //return hashedFile;
    }

    public  void saveKey(GnuRSAPrivateKey privKey, GnuRSAPublicKey pubKey, String privKeyFile, String pubKeyFile) {
        byte[] privKeyBytes = privKey.getEncoded(IKeyPairCodec.RAW_FORMAT);
        CryptoUtil.writeToFile(mContext, privKeyBytes, new File(privKeyFile));
        byte[] pubKeyBytes = pubKey.getEncoded(IKeyPairCodec.RAW_FORMAT);
        CryptoUtil.writeToFile(mContext, pubKeyBytes, new File(pubKeyFile));
    }
}
