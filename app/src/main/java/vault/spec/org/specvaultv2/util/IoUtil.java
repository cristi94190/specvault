package vault.spec.org.specvaultv2.util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;

/**
 * Created by Loser on 10.08.2016.
 */
public class IoUtil {
    public static void throwThrowable(Throwable argT){
        try{
            throw argT;
        }catch(Throwable t){
            Log.d(IoUtil.class.getName(), "" + t.getMessage());
        }
    }
    public static void closeStream(Closeable argStream) {
        try {
            argStream.close();
        } catch (Throwable t) {
            Log.d(IoUtil.class.getName(), "" + t.getMessage());
        }
    }

    public static File getAppDir(Context argContext) {
        File file = new File(argContext.getExternalFilesDir(null), "Spec");
        checkDirectory(file);
        return file;
    }

    public static  File getPasswordHashDirectory(Context argContext){
        File root = new File(getAppDir(argContext),"Pass");
        checkDirectory(root);
        return root;
    }

    public static  File getPasswordHashFile(Context argContext){
        File root = new File(getPasswordHashDirectory(argContext),"hash.bin");
        return root;
    }

    public static File getKeyDirectory(Context argContext){
        File dir = new File(getAppDir(argContext), "Key");
        checkDirectory(dir);
        return dir;
    }

    public static File getPublicKeyFile(Context argContext){
        File pubKeyFile = new File(getKeyDirectory(argContext), "pub.bin");
        return pubKeyFile;
    }

    public static File getPrivateKeyFile(Context argContext){
        File privKeyFile = new File(getKeyDirectory(argContext), "priv.bin");
        return privKeyFile;
    }

    public static File getAesKeyFile(Context argContext){
        File privKeyFile = new File(getKeyDirectory(argContext), "aesKey.bin");
        return privKeyFile;
    }

    public static void checkDirectory(File argFile) {
        boolean dirsCreated = true;

        if (argFile.exists() == true && argFile.isDirectory() == false) {
            throw new RuntimeException("Filesystem item " + argFile.getAbsolutePath() + " is not a directory.");
        }
        if (argFile.exists() == false) {
            dirsCreated = argFile.mkdirs();
        }
        if (dirsCreated == false) {
            throw new RuntimeException("Directory " + argFile.getAbsolutePath() + " could not be created.");
        }
    }

    public static void store(final BigInteger values,
                              final OutputStream sink) throws IOException {
        ObjectOutputStream stream;
        stream = null;
        try {
            stream = new ObjectOutputStream(sink);
            stream.writeObject(values);
        }catch(Throwable t){
            Log.e("Writing Key", "Error writing key");
        } finally {
            if(stream != null) {
                stream.close();
            }
        }
    }

}
