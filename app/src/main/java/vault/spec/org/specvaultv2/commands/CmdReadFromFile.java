package vault.spec.org.specvaultv2.commands;

import android.util.Base64;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import rsa.PrintUtil;

import vault.spec.org.specvaultv2.ApplicationController;

/**
 * Created by radu on 08.08.2016.
 */
public class CmdReadFromFile extends AbstractCommand {
    private String mFilePath;
    private byte[] mPasswordBytes;

    public CmdReadFromFile(String argFilePath){
        mFilePath = argFilePath;
    }

    @Override
    public void execute(){
        /*File file = new File(mFilePath);
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] passwordBytes = new byte[]{};
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        if(file.length() < 20000){
            dataInputStream.readFully(passwordBytes);
            mPasswordBytes = passwordBytes;
        }*/

    }

    public byte[] getPass(){
        return mPasswordBytes;
    }

    public String getStringPass(){
        return Base64.encodeToString(mPasswordBytes, 0);
    }

    @Override
    public String describeCommand() {
        return "Reading password from file ...";
    }
}
