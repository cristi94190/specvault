package vault.spec.org.specvaultv2.commands;

import android.content.Context;

import vault.spec.org.specvaultv2.ApplicationModel;

/**
 * Created by Loser on 10.08.2016.
 */

public abstract class AbstractCommand {
    protected Context mContext;
    protected ApplicationModel mModel;
    protected AbstractCommand(){
        this.mContext = null;
        this.mModel = null;
    }

    public AbstractCommand(Context mContext, ApplicationModel mModel) {
        this.mContext = mContext;
        this.mModel = mModel;
    }

    public abstract void execute() throws Throwable;
    public abstract String describeCommand();

}