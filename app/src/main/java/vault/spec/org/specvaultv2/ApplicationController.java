package vault.spec.org.specvaultv2;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import vault.spec.org.specvaultv2.commands.AbstractCommand;
import vault.spec.org.specvaultv2.commands.CmdAppInit;
import vault.spec.org.specvaultv2.commands.CmdCreateVault;
import vault.spec.org.specvaultv2.commands.CmdEncryptFile;
import vault.spec.org.specvaultv2.commands.CmdGetFiles;
import vault.spec.org.specvaultv2.commands.CmdHash;
import vault.spec.org.specvaultv2.commands.CmdLogin;
import vault.spec.org.specvaultv2.commands.CmdReadFromFile;
import vault.spec.org.specvaultv2.commands.CmdResetPass;
import vault.spec.org.specvaultv2.commands.CmdWriteToFile;

/**
 * Created by radu on 08.08.2016.
 */
public class ApplicationController {
    private ApplicationModel mAppModel;
    private Worker mWorker;
    private Context mContext;

    public ApplicationController(ApplicationModel argModel, Context argContext) {
        this.mAppModel = argModel;
        this.mContext = argContext;
        mWorker = null;
    }

    public void commandInitialisation() {
        CmdAppInit command = new CmdAppInit(this.mContext,this.mAppModel);
        ExecCommand(command);
    }

    public void commandEncrypy(File argFile, String argPass){
        CmdEncryptFile command = new CmdEncryptFile(this.mContext, this.mAppModel,  argFile, argPass);
        ExecCommand(command);
    }

    public void commandDecrypt(File argFile, String argPass){
        CmdEncryptFile command = new CmdEncryptFile(this.mContext, this.mAppModel,  argFile, argPass);
        ExecCommand(command);
    }

    public void commandCreateVault(String argPassword, String argRepeatPassword) {
        CmdCreateVault command = new CmdCreateVault(this.mContext, this.mAppModel, argPassword, argRepeatPassword);
        ExecCommand(command);
    }

    public void commandLogin(String argPassword) {
        CmdLogin command = new CmdLogin(this.mContext, this.mAppModel, argPassword);
        ExecCommand(command);
    }

    public void commandGetFiles(String argPath) {
        CmdGetFiles command = new CmdGetFiles(this.mContext, this.mAppModel, argPath);
        ExecCommand(command);
    }

    //readFromFile:
    public String readFromFileCommand(){
        CmdReadFromFile command = new CmdReadFromFile("pass.txt");
        ExecCommand(command);
        return Base64.encodeToString(command.getPass(), Base64.DEFAULT);
    }

    public String HashBytes(byte[] passBytes){
        CmdHash command = new CmdHash(passBytes);
        ExecCommand(command);
        return command.getHashedString();
    }

    public String HashBytes(String passString){
        CmdHash command = new CmdHash(passString);
        ExecCommand(command);
        return command.getHashedString();
    }

    public void resetPass(){
        CmdResetPass command = new CmdResetPass(this.mContext, this.mAppModel);
        ExecCommand(command);
    }

    public void writeToFile(String pass, String path, Context ctx){
        CmdWriteToFile command = new CmdWriteToFile(pass, path, ctx);
        ExecCommand(command);
    }



    public void ExecCommand(AbstractCommand command) {
        if (command == null) return;
        if (mWorker == null) {
            ExecuteInternal(command);
        } else {
            boolean runningOrPending = mWorker.getStatus() == AsyncTask.Status.RUNNING || mWorker.getStatus() == AsyncTask.Status.PENDING;
            if (runningOrPending) return;
            ExecuteInternal(command);
        }
    }

    private void ExecuteInternal(AbstractCommand command) {
        mWorker = new Worker(command);
        mWorker.execute();
    }



    protected class Worker extends AsyncTask<Void, String, Void> {
        Throwable mError;
        AbstractCommand mCommand;

        public Worker(AbstractCommand argCommand) {
            mCommand = argCommand;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mAppModel.getmListener().onCommandStarted(mAppModel, mCommand.describeCommand());
        }

        @Override
        protected Void doInBackground(Void... iComands) {
            try {
                this.mCommand.execute();
            } catch (Throwable t) {
                mError = t;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mAppModel.getmListener().onCommandFinished(mAppModel, mError);
        }


    }

}
