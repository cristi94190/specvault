package vault.spec.org.specvaultv2.commands;

import android.content.Context;

import java.io.File;

import vault.spec.org.specvaultv2.ApplicationController;
import vault.spec.org.specvaultv2.ApplicationModel;
import vault.spec.org.specvaultv2.util.IoUtil;

/**
 * Created by radu on 09.08.2016.
 */
public class CmdResetPass extends AbstractCommand {

    public CmdResetPass(Context argCtx, ApplicationModel argModel){
        super(argCtx, argModel);
    }

    @Override
    public void execute(){
        File file = IoUtil.getPasswordHashFile(mContext);
        if(file.exists()) {
            boolean deleted = file.delete();
        }
        mModel.setmState(ApplicationModel.STATE_REGISTERING);
    }

    @Override
    public String describeCommand() {
        return "Resetting PASSWORD";
    }
}
