package vault.spec.org.specvaultv2;

/**
 * Created by radu on 08.08.2016.
 */
public class ApplicationModel {

    public final static int STATE_INITIAL = 0;
    public final static int STATE_LOGGING_IN = 1;
    public final static int STATE_REGISTERING = 2;
    public final static int STATE_VAULT_BROWSING = 3;
    public final static int STATE_CHANGING_PASSWORD = 4;

    private int mState;
    private Listener mListener;

    public interface Listener {
        void onCommandStarted(ApplicationModel argModel, String argMessage);
        void onCommandFinished(ApplicationModel argModel, Throwable argThrowable);
    }

    public ApplicationModel(Listener argListener) {
        mState = STATE_INITIAL;
        mListener = argListener;
    }

    public void validateState() {
        if (mState < 0 || mState > 4) {
            throw new RuntimeException("Invalid state reached.");
        }
    }

    public synchronized int getmState() {
        return mState;
    }

    public synchronized void setmState(int mState) {
        this.mState = mState;
    }

    public Listener getmListener() {
        return mListener;
    }

    public void setmListener(Listener mListener) {
        this.mListener = mListener;
    }
}
