package vault.spec.org.specvaultv2.util;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;

import digest.Sha256;

/**
 * Created by Loser on 14.08.2016.
 */
public class CryptoUtil {

    public static byte[] derivPass(String pass, int numIter) {
        byte[] hashedBytes = pass.getBytes();
        for (int i = 0; i < numIter; i++) {
            Sha256 sha = new Sha256();
            sha.update(hashedBytes, 0, hashedBytes.length);
            hashedBytes = sha.digest();
        }
        return Arrays.copyOfRange(hashedBytes, 0, 16);
    }

    public static void writeToFile(Context argContext, byte[] argBytes, File argFile) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(argFile);
            fos.write(argBytes);

        }catch (Throwable t){
            Log.e("Writing", "Error writing to file" + t.getMessage());
        }finally{
            IoUtil.closeStream(fos);
        }
    }
}
