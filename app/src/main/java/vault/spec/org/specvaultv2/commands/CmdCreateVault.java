package vault.spec.org.specvaultv2.commands;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;

import digest.Sha256;
import vault.spec.org.specvaultv2.ApplicationModel;
import vault.spec.org.specvaultv2.util.IoUtil;

/**
 * Created by Loser on 10.08.2016.
 */
public class CmdCreateVault extends AbstractCommand {
    String mPassword;
    String mRepeatPassword;
    public CmdCreateVault(Context argContext, ApplicationModel argModel, String argPassword, String argRepeatPassword){
        super(argContext,argModel);
        this.mRepeatPassword = argRepeatPassword;
        this.mPassword = argPassword;
    }
    @Override
    public void execute() {
        if(mPassword.compareTo(mRepeatPassword)!=0){
            throw new RuntimeException("Passwords missmatch.");
        }else{
            File hashFile = IoUtil.getPasswordHashFile(mContext);
            FileOutputStream fos = null;
            try{
                fos = new FileOutputStream(hashFile);
                byte[] hashedPass = hash(mPassword, 10);
                fos.write(hashedPass);
                mModel.setmState(ApplicationModel.STATE_LOGGING_IN);
            }catch(Throwable t){
                Log.e("Writing", "Hash file could not be created");
            }finally {
             IoUtil.closeStream(fos);
            }
        }
    }

    public byte[] hash(String argString, int numberOfIterations){
        Sha256 sha = new Sha256();
        byte[] bytes = argString.getBytes();
        if(bytes.length > 0){
            for(int i = 0; i < numberOfIterations; i++)
            sha.update(bytes, 0, bytes.length);
            bytes = sha.digest();
        }
        return bytes;
    }

    @Override
    public String describeCommand() {
        return "Creating vault...";
    }
}
