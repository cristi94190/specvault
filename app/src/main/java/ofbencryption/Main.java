/*
 * Copyright (C) 2016 Student
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ofbencryption;

import digest.Sha256;
import digest.Sha512;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import rsav2.RSAv2;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
//import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
//import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import rsa.PrintUtil;
import static rsa.PrintUtil.toHex;
import rsa.RSA;
import rsa.key.GnuRSAPrivateKey;
import rsa.key.GnuRSAPublicKey;
import rsa.key.IKeyPairCodec;
import rsa.key.RSAKeyPairGenerator;
import static rsa.key.RSAKeyPairGenerator.MODULUS_LENGTH;

/**
 *
 * @author Student
 */
public class Main {

    final static String timeStamp = "" + System.currentTimeMillis();
    final static String plainFile = "C:\\Users\\Student\\Downloads\\input.txt";
    final static String encryptedFile = plainFile + "." + timeStamp + ".enc";
    final static String decryptedFile = plainFile + "." + timeStamp + ".dec";
    static String key = "``00111100001111";
    final static String iv = "00000000000000000011100000000000";
    final static String key1 = "0000111100001111";
    final static String pass = "123";
    final static long MAX_LENGTH = 20000;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        //encrypt();
        //decrypt();
        /*
        //if file is empty, generate key, else readFromFile
        RSAKeyPairGenerator keygen = new RSAKeyPairGenerator();
        Map att = new HashMap();
        RSAKeyGenParameterSpec RSASpec = new RSAKeyGenParameterSpec(1024, new BigInteger("12"));
        //att.put(RSA_PARAMETERS, RSASpec);
        att.put(MODULUS_LENGTH,  1024);
        keygen.setup(att);
        
        use loadKeyPair(PublicKey, PrivateKey) 

        //use loadKeyPair(PublicKey, PrivateKey) to load already stored keys
        KeyPair kp1 = keygen.generate();
        keygen.setup(att);
        
        byte[] bytes = //PrintUtil.randomString(32).getBytes();
                        key.getBytes();
        BigInteger bi = new BigInteger(bytes);
         */
        //KeyPair kp = RSAGenerateKeyPair();

        //saveKey((GnuRSAPrivateKey)kp.getPrivate(),(GnuRSAPublicKey) kp.getPublic(), "privKey.txt", "pubKey.txt");
        String text = "Choose something:";
        text = text + "\n1.Generate a new key pair.";
        text = text + "\n2.Load the key pair from file";
        text = text + "\n Your choice: ";
        System.out.println(text);
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String s = bufferRead.readLine();
        
        //
        byte[] keyBytes = derivPass(pass, 15);
        System.out.println("KeyToHex: " + PrintUtil.toHex(keyBytes));
        //System.out.println("KeyToHex: " + PrintUtil.toHex(key.getBytes()));
        
        KeyPair kp;
        
        if (Integer.parseInt(s) == 1) {
            kp = RSAGenerateKeyPair();
            saveKey((GnuRSAPrivateKey) kp.getPrivate(), (GnuRSAPublicKey) kp.getPublic(), "privKey.txt", "pubKey.txt");
        }
        else 
            kp = loadKeyPair("privKey.txt", "pubKey.txt");

        kp.getPublic().getEncoded();//
        BigInteger encrypted = RSAEncrypt(keyBytes, kp);
        //System.out.println("BigInteger key:                     " + bi);
        //BigInteger encrypted = RSA.encrypt(kp1.getPublic(), bi);
        System.out.println("EncryptedKey:                       " + PrintUtil.toHex(encrypted.toByteArray()));
        System.out.println("EncryptedKey:                       " + PrintUtil.toHex(encrypted.toByteArray()).length());

        BigInteger decrypted
                = //RSA.decrypt(kp1.getPrivate(), encrypted);
                RSADecrypt(kp, encrypted);
        System.out.println("Decrypted Key(BigInteger):          " + decrypted);
        System.out.println("Decrypted Key(Hex from byteArray):  " + PrintUtil.toHex(decrypted.toByteArray()));
        System.out.println("Original:                           " + PrintUtil.toHex(keyBytes));
        System.out.println("Original length:                    " + PrintUtil.toHex(keyBytes).length());

        String AesEncryptedFile = "AesEncryptedFile" + timeStamp + ".txt";
        //encrypt(PrintUtil.toHex(decrypted.toByteArray()), "plainFile.txt", AesEncryptedFile);
        encrypt(decrypted.toByteArray(), "plainFile.txt", AesEncryptedFile);

        byte[] byteToWrite = encrypted.toByteArray();
        PrintUtil.writeBytes("RSAEncryptedKey.txt", byteToWrite);
        String AesDecryptedFile = "DecryptedFile" + timeStamp + ".txt";
        decrypt(decrypted.toByteArray(), AesDecryptedFile, AesEncryptedFile);

        //digest:
        String hashAfterDecryption = toHex(hashFile(AesDecryptedFile));
        System.out.println("Hashed File (AfterDecryption):  " + hashAfterDecryption);
        String hashPlainFile = toHex(hashFile("plainFile.txt"));
        System.out.println("Hashed File (Original File:)    " + hashPlainFile);
        check(hashAfterDecryption, hashPlainFile);

        //sha512:
        Sha512 sha512 = new Sha512();
        byte[] byteIn = new byte[]{1, 2, 3};
        byte[] hashed;
        sha512.update(byteIn, 0, 0);
        hashed = sha512.digest();
        System.out.println("Sha512 input: " + toHex(byteIn));
        System.out.println("Sha512 output:" + toHex(hashed));
        /*      RSAv2 rsa = new RSAv2(1024);
        BigInteger bi = new BigInteger(new byte[]{33,22,33,11,22,11,22,127,109,98});
        System.out.println(RSAv2.toHex(bi.toByteArray()));
        BigInteger enc = rsa.encrypt(bi);
        System.out.println(RSAv2.toHex(enc.toByteArray()));
        System.out.println(RSAv2.toHex(enc.toByteArray()).length());
        BigInteger dec = rsa.decrypt(enc);
        System.out.println(RSAv2.toHex(dec.toByteArray()));
         */
    }

    /*private static void encrypt(String argKey, String argPlaintextFile, String argEncryptedFile) throws Exception {

        CryptoModule encryptionModule = new CryptoModule(true, argPlaintextFile, argEncryptedFile, argKey, iv, new CryptoModule.ProgressListener() {
            @Override
            public void onProgress(int argPercent) {
                System.out.println("Encryption progress report: " + argPercent + " % .");
            }
        });
        encryptionModule.init();
        encryptionModule.process();
    }

    private static void decrypt(String argKey, String argDecryptedFile, String argEncryptedFile) throws Exception {
        CryptoModule decryptionModule = new CryptoModule(false, argEncryptedFile, argDecryptedFile, argKey, iv, new CryptoModule.ProgressListener() {
            @Override
            public void onProgress(int argPercent) {
                System.out.println("Decryption progress report: " + argPercent + " % .");
            }
        });
        decryptionModule.init();
        decryptionModule.process();
    }
     */
    private static void encrypt(byte[] argKey, String argPlaintextFile, String argEncryptedFile) throws Exception {

        CryptoModule encryptionModule = new CryptoModule(true, argPlaintextFile, argEncryptedFile, argKey, iv, new CryptoModule.ProgressListener() {
            @Override
            public void onProgress(int argPercent) {
                //System.out.flush();
                System.out.println("Encryption progress report: " + argPercent + " % .");
            }
        });
        encryptionModule.init();
        encryptionModule.process();
        //byte[] hashedFile = encryptionModule.process();
        //return hashedFile;
    }

    private static void decrypt(byte[] argKey, String argDecryptedFile, String argEncryptedFile) throws Exception {
        CryptoModule decryptionModule = new CryptoModule(false, argEncryptedFile, argDecryptedFile, argKey, iv, new CryptoModule.ProgressListener() {
            @Override
            public void onProgress(int argPercent) {
                System.out.flush();
                System.out.println("Decryption progress report: " + argPercent + " % .");
            }
        });
        decryptionModule.init();
        decryptionModule.process();
    }

     public static KeyPair RSAGenerateKeyPair() {
        //if file is empty, generate key, else readFromFile
        RSAKeyPairGenerator keygen = new RSAKeyPairGenerator();
        Map att = new HashMap();
        //RSAKeyGenParameterSpec RSASpec = new RSAKeyGenParameterSpec(1024, new BigInteger("12"));
        //att.put(RSA_PARAMETERS, RSASpec);
        att.put(MODULUS_LENGTH, 1024);
        keygen.setup(att);
        keygen.setup(att);
        KeyPair kp1 = keygen.generate();

        return kp1;
    }

    private static BigInteger RSAEncrypt(byte[] argBytes, KeyPair kp1) {

        //byte[] bytes = //PrintUtil.randomString(32).getBytes();
        BigInteger bi = new BigInteger(argBytes);
        bi = bi.abs();
        System.out.println("BigInteger key:                     " + bi);
        BigInteger encrypted = RSA.encrypt(kp1.getPublic(), bi);
        //System.out.println("EncryptedKey:                       " + PrintUtil.toHex(encrypted.toByteArray()));
        return encrypted;
    }

    private static BigInteger RSADecrypt(KeyPair kp1, BigInteger encrypted) {
        BigInteger decrypted = RSA.decrypt(kp1.getPrivate(), encrypted);
        return decrypted;
    }

    /*private static BigInteger RSAEncrypt(PublicKey pubKey, PrivateKey privKey, byte[] argBytes) throws Throwable {
        RSAKeyPairGenerator keygen = new RSAKeyPairGenerator();
        KeyPair kp1 = loadKeyPair("keyPair", "RSA");

        //byte[] bytes = //PrintUtil.randomString(32).getBytes();
        BigInteger bi = new BigInteger(argBytes);
        System.out.println("BigInteger key:                     " + bi);
        BigInteger encrypted = RSA.encrypt(kp1.getPublic(), bi);
        System.out.println("EncryptedKey:                       " + PrintUtil.toHex(encrypted.toByteArray()));
        return encrypted;
    }
    */
    
    private static byte[] hashFile(String argFilename) throws FileNotFoundException, IOException {
        Sha256 sha = new Sha256();
        byte[] hashedBytes;
        byte[] dataIn = new byte[16];
        FileInputStream fileInputStream = new FileInputStream(argFilename);
        int bytesCount;
        while (true) {
            bytesCount = fileInputStream.read(dataIn);
            if (bytesCount == -1) {
                break;
            }
            sha.update(dataIn, 0, bytesCount);
        }
        hashedBytes = sha.digest();
        return hashedBytes;
    }

    public static boolean check(String argFileHash, String argFileHash2) {
        if (argFileHash.equalsIgnoreCase(argFileHash2)) {
            System.out.println("The hashes are equal, integrity check succesful.");
            return true;
        } else {
            System.out.println("The hashes are not equal, integrity check failed.");
            return false;
        }
    }

    /*
    public static void saveKeyPair(String privateFile, String publicFile, KeyPair keyPair) {
        try {
            PrivateKey privateKey = keyPair.getPrivate();
            PublicKey publicKey = keyPair.getPublic();

            // Store Public Key.
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey.getEncoded());
            FileOutputStream fos = new FileOutputStream(publicFile);
            fos.write(x509EncodedKeySpec.getEncoded());
            fos.close();

            // Store Private Key.
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());
            fos = new FileOutputStream(privateFile);
            fos.write(pkcs8EncodedKeySpec.getEncoded());
            fos.close();
        } catch (Throwable t) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, t);
        }
    }
*/
    public static RSAPublicKey readPublicKey(String filename) {
        try {
            byte[] fileBytes = readBytes(filename);
            GnuRSAPublicKey pubKey = GnuRSAPublicKey.valueOf(fileBytes);
            return pubKey;
        } catch (Throwable t) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, t);
        }
        return null;
    }

    public static RSAPrivateKey readPrivateKey(String filename) {
        try {
            byte[] fileBytes = readBytes(filename);
            GnuRSAPrivateKey privKey = GnuRSAPrivateKey.valueOf(fileBytes);
            return privKey;
        } catch (Throwable t) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, t);
        }
        return null;
    }

    public static KeyPair loadKeyPair(String privateKeyPath, String publicKeyPath) {
        return new KeyPair(readPublicKey(publicKeyPath), readPrivateKey(privateKeyPath));

    }
    
    public static byte[] readByte(String argFilename)throws FileNotFoundException, IOException{
        File f = new File(argFilename);
        FileInputStream fis = new FileInputStream(f);
        byte[] keyBytes = null;
        fis.read(keyBytes);
      
        return keyBytes;
        
    }
    public static byte[] readBytes(String argFilename) throws FileNotFoundException, IOException {
        File f = new File(argFilename);
        FileInputStream fis = new FileInputStream(f);
        if(f.length() < MAX_LENGTH){
            try {
                DataInputStream dis = new DataInputStream(fis);
                byte[] keyBytes = new byte[(int) f.length()];
                dis.readFully(keyBytes);
                dis.close();
                return keyBytes;
            } catch (Throwable ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                fis.close();
            }
        }else{
            System.out.println("File is to large");
        }
        return null;
    }

    public static void writeBytes(byte[] argBytes, String argFilename) throws FileNotFoundException {
        File f = new File(argFilename);
        FileOutputStream fos = new FileOutputStream(f);
        try {
            fos.write(argBytes, 0, argBytes.length);
        } catch (Throwable t) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, t);
        }
    }

    public static void saveKey(GnuRSAPrivateKey privKey, GnuRSAPublicKey pubKey, String privKeyFile, String pubKeyFile) {
        try {
            byte[] privKeyBytes = privKey.getEncoded(IKeyPairCodec.RAW_FORMAT);
            writeBytes(privKeyBytes, privKeyFile);
            byte[] pubKeyBytes = pubKey.getEncoded(IKeyPairCodec.RAW_FORMAT);
            writeBytes(pubKeyBytes, pubKeyFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static byte[] derivPass(String pass, int numIter){
        byte[] hashedBytes = pass.getBytes();
        for (int i = 0; i < numIter; i++){
            Sha256 sha = new Sha256();
            sha.update(hashedBytes, 0, hashedBytes.length);
            hashedBytes = sha.digest();   
        }
    return Arrays.copyOfRange(hashedBytes, 0, 16);
    }
}
