/*
 * Copyright (C) 2016 CP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ofbencryption;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Student
 */
public class FileInOut {

    public static byte[] readBytes(File argFile) throws IOException {
        FileInputStream fis = null;

        fis = new FileInputStream(argFile);
        byte content[] = new byte[(int) argFile.length()];
        fis.read(content);
        
        if (fis != null) {
            fis.close();
        }
        return content;
    }

    public static void writeBytes(String argFilePath, byte[] argByteArray) throws IOException {
        FileOutputStream fos = new FileOutputStream(argFilePath);

        fos.write(argByteArray);
        if (fos != null) {
            fos.close();
        }
    }
}
