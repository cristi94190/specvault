/*
 * Copyright (C) 2016 Student
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ofbencryption;

import core.Util;
import core.alg.IBlockCipher;
import core.alg.Rijndael;
import core.mode.IMode;
import core.mode.OFB;
import digest.Sha256;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import rsa.PrintUtil;

/**
 *
 * @author Student
 */
public class CryptoModule {

    private String mInputFile;
    private String mOutputFile;
    //private String mKey;
    private byte[] mKey;
    private String mIv;
    private boolean mForEncryption;
    private ProgressListener mProgressListener;
    private OFB ofb;
    private int mProgress;

    /*public CryptoModule(boolean mForEncryption, String mInputFile, String mOutputFile, String mKey, String mIv, ProgressListener argListener) {
        this.mInputFile = mInputFile;
        this.mOutputFile = mOutputFile;
        this.mKey = mKey;
        this.mIv = mIv;
        this.mForEncryption = mForEncryption;
        this.mProgressListener = argListener;
        this.ofb = null;
        this.mProgress = -1;
    }
    */
    
    public CryptoModule(boolean mForEncryption, String mInputFile, String mOutputFile, byte[] mKey, String mIv, ProgressListener argListener) {
        this.mInputFile = mInputFile;
        this.mOutputFile = mOutputFile;
        this.mKey = mKey;
        this.mIv = mIv;
        this.mForEncryption = mForEncryption;
        this.mProgressListener = argListener;
        this.ofb = null;
        this.mProgress = -1;
    }

    public void init() throws Exception {
        validate();
      
        byte[] key = this.mKey;
        byte[] iv = Util.toBytesFromString(this.mIv);
        Map attributes = new HashMap();
        attributes.put(IBlockCipher.CIPHER_BLOCK_SIZE, 16);
        attributes.put(IBlockCipher.KEY_MATERIAL, key);
        attributes.put(IMode.IV, iv);
        attributes.put(IMode.STATE, mForEncryption == true ? IMode.ENCRYPTION : IMode.DECRYPTION);
        ofb = new OFB(new Rijndael(), 16);
        ofb.init(attributes);
    }

    private void validate() throws CryptoModuleException {

        File inFile = new File(mInputFile);
        File outFile = new File(mOutputFile);
        //&& PrintUtil.toHex(mKey).length() != 14
        if (PrintUtil.toHex(mKey).length() != 32 && PrintUtil.toHex(mKey).length() != 64 && PrintUtil.toHex(mKey).length() != 56 ) {
            throw new CryptoModuleException("Invalid key length.");
        }
        if (!PrintUtil.toHex(this.mKey).matches("[0-9A-F]+")) {
            throw new CryptoModuleException("Key is not hex-encoded.");
        }

        if (mIv.length() != 32) {
            throw new CryptoModuleException("Invalid key length.");
        }
        if (!this.mIv.matches("[0-9A-F]+")) {
            throw new CryptoModuleException("Key is not hex-encoded.");
        }
        if (!inFile.exists()) {
            throw new CryptoModuleException("File does not exist.");
        }
        if (inFile.isDirectory()) {
            throw new CryptoModuleException("File is a directory.");
        }
        if (inFile.length() == 0) {
            throw new CryptoModuleException("File is empty.");
        }
        if (outFile.exists()) {
            throw new CryptoModuleException("You are not allowed to overwrite existing files.");
        }

    }

    public void process() throws IOException, CryptoModuleException {
          
        //Sha256 sha = new Sha256();
        //byte[] hashedBytes;
        
        if (this.ofb == null) {
            throw new CryptoModuleException("OFB is null, please call the init method.");
        }
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        long currentBytes;
        long maxBytes;
        try {
            File inFile = new File(mInputFile);
            File outFile = new File(mOutputFile);
            currentBytes = 0;
            maxBytes = inFile.length();
            fileInputStream = new FileInputStream(inFile);
            fileOutputStream = new FileOutputStream(outFile);
            byte[] dataIn = new byte[16];
            byte[] dataOut = new byte[16];
            while (true) {
                int bytesCount = fileInputStream.read(dataIn);
                if (bytesCount == -1) {
                    break;
                }
                /*if (this.mForEncryption == false) {
                    onCipherTextDataAvailable(dataIn, 0, bytesCount);
                } 
                 */
                //criptare aes ofb
                ofb.update(dataIn, 0, dataOut, 0);
                //if (this.mForEncryption == true) {
                //    onCipherTextDataAvailable(dataIn, 0, bytesCount);
                //} 
                //scriere date criptatte
                
                //Hash:
                
                //sha.update(dataIn, 0, bytesCount);

                fileOutputStream.write(dataOut, 0, bytesCount);
                //progress bar
                currentBytes += bytesCount;
                int currentProgress = (int) ((currentBytes * 100) / maxBytes);
                if (currentProgress != mProgress) {
                    mProgress = currentProgress;
                    mProgressListener.onProgress(mProgress);
                }
            }
            //
            //hashedBytes = sha.digest();
            //return hashedBytes;
        } finally {
            closeStream(fileInputStream);
            closeStream(fileOutputStream);
        }

    }

    
    public void processEnc() throws IOException, CryptoModuleException {
          
        Sha256 sha = new Sha256();
        byte[] hashedBytes;
        
        if (this.ofb == null) {
            throw new CryptoModuleException("OFB is null, please call the init method.");
        }
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        long currentBytes;
        long maxBytes;
        try {
            File inFile = new File(mInputFile);
            File outFile = new File(mOutputFile);
            currentBytes = 0;
            maxBytes = inFile.length();
            fileInputStream = new FileInputStream(inFile);
            fileOutputStream = new FileOutputStream(outFile);
            byte[] dataIn = new byte[16];
            byte[] dataOut = new byte[16];
            while (true) {
                int bytesCount = fileInputStream.read(dataIn);
                if (bytesCount == -1) {
                    break;
                }
                /*if (this.mForEncryption == false) {
                    onCipherTextDataAvailable(dataIn, 0, bytesCount);
                } 
                 */
                //criptare aes ofb
                ofb.update(dataIn, 0, dataOut, 0);
                //if (this.mForEncryption == true) {
                //    onCipherTextDataAvailable(dataIn, 0, bytesCount);
                //} 
                //scriere date criptatte

                fileOutputStream.write(dataOut, 0, bytesCount);
                //progress bar
                currentBytes += bytesCount;
                int currentProgress = (int) ((currentBytes * 100) / maxBytes);
                if (currentProgress != mProgress) {
                    mProgress = currentProgress;
                    mProgressListener.onProgress(mProgress);
                }
            }
            //
        } finally {
            closeStream(fileInputStream);
            closeStream(fileOutputStream);
        }

    }
    private void closeStream(FileOutputStream argFileOutputStream) {
        try {
            argFileOutputStream.flush();
            argFileOutputStream.close();
        } catch (IOException ex) {

        }
    }

    private void closeStream(FileInputStream argFileInputStream) {
        try {
            argFileInputStream.close();
        } catch (IOException ex) {

        }
    }

    private void onCipherTextDataAvailable(byte[] dataIn, int i, int bytesCount) {
        
    }

    public interface ProgressListener {

        void onProgress(int argPercent);
    }

}
